class MyDefine {
  static const String MISSING_IMG =
      "https://mortgage-magic.co.uk/api/content/media/default_avatar.png";
  static const EMAIL_KEY = "";

  static const CUS_CHATID_PREFIX = "Cus-";
  static const SP_CHATID_PREFIX = "SP-";

  //  ********************************************************  TEST

  static const test_lat = -36.826511;
  static const test_lng = 174.763753;
  static const test_geo_code = "uk";

  static const ORDINAL_NOS = [
    '1st',
    '2nd',
    '3rd',
    '4th',
    '5th',
    '6th',
    '7th',
    '8th',
    '9th',
    '10th',
    '11th',
    '12th',
    '13th',
    '14th',
    '15th',
    '16th',
    '17th',
    '18th',
    '19th',
    '20th',
    '21st',
    '22nd',
    '23rd',
    '24th',
    '25th',
    '26th',
    '27th',
    '28th',
    '29th',
    '30th',
    '31st'
  ];
}
