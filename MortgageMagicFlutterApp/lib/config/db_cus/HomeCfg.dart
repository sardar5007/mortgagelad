import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';

class HomeCfg {
  static final Color completedColor = Colors.red[900];
  static final Color fmaColor = MyTheme.pinkColor;
  static final offeredColor = Colors.redAccent.shade100;
  static final Color leadColor = Colors.grey[400].withOpacity(0.8);
}
