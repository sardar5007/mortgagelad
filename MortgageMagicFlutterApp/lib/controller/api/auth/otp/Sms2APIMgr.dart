import 'package:aitl/Mixin.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/auth/otp/LoginMobOtpPostHelper.dart';
import 'package:aitl/controller/helper/auth/otp/SendOtpNotiHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/auth/otp/MobileUserOtpPostAPIModel.dart';
import 'package:aitl/model/json/auth/otp/SendOtpNotiAPIModel.dart';
import 'package:flutter/cupertino.dart';

class Sms2APIMgr with Mixin {
  static final Sms2APIMgr _shared = Sms2APIMgr._internal();

  factory Sms2APIMgr() {
    return _shared;
  }

  Sms2APIMgr._internal();

  wsLoginMobileOtpPostAPI({
    BuildContext context,
    String mobile,
    Function(MobileUserOtpPostAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<MobileUserOtpPostAPIModel, Null>(
        context: context,
        url: Server.LOGIN_MOBILE_OTP_POST_URL,
        param: LoginMobOtpPostHelper().getParam(
          mobileNumber: mobile,
        ),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }

  wsSendOtpNotiAPI({
    BuildContext context,
    int otpId,
    Function(SendOtpNotiAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<SendOtpNotiAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: SendOtpNotiHelper().getUrl(otpId: otpId),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }
}
