import 'package:aitl/Mixin.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/tab_more/support/ResolutionAPIModel.dart';
import 'package:flutter/material.dart';
import 'package:aitl/model/json/db_cus/case_lead/CaseLeadNegotiatorUserAPIModel.dart';

class CaseLeadNavigatorAPIMgr with Mixin {
  static final CaseLeadNavigatorAPIMgr _shared =
      CaseLeadNavigatorAPIMgr._internal();

  factory CaseLeadNavigatorAPIMgr() {
    return _shared;
  }

  CaseLeadNavigatorAPIMgr._internal();

  wsGetCaseLeadNavigatorByIntroducerID(
      {BuildContext context,
      // UserNotesModel userNotesModel,
      Function(CaseLeadNegotiatorUserAPIModel) callback}) async {
    try {
      await NetworkMgr()
          .req<CaseLeadNegotiatorUserAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: Server.CASELEAD_NAVIGATOR_GET_URL
            .replaceAll("#introducerId#", "115774"),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }

  wsResolutionAPI({
    BuildContext context,
    dynamic param,
    Function(ResolutionAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<ResolutionAPIModel, Null>(
        context: context,
        url: Server.RESOLUTION_URL,
        param: param,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
