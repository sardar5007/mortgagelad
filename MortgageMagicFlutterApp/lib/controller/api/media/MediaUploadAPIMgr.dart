import 'dart:io';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:flutter/cupertino.dart';

class MediaUploadAPIMgr with Mixin {
  static final MediaUploadAPIMgr _shared = MediaUploadAPIMgr._internal();

  factory MediaUploadAPIMgr() {
    return _shared;
  }

  MediaUploadAPIMgr._internal();

  wsMediaUploadFileAPI({
    BuildContext context,
    File file,
    Function(MediaUploadFilesAPIModel) callback,
  }) async {
    try {
      await NetworkMgr().uploadFiles<MediaUploadFilesAPIModel, Null>(
        context: context,
        url: Server.MEDIA_UPLOADFILES_URL,
        files: [file],
      ).then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
