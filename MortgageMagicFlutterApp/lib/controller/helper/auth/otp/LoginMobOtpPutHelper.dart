class LoginMobOtpPutHelper {
  getParam({
    String mobileNumber,
    String otpCode = '',
  }) {
    return {
      "MobileNumber": mobileNumber,
      "OTPCode": otpCode,
    };
  }
}
