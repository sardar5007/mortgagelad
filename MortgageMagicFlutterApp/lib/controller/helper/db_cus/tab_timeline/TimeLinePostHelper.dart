import 'package:aitl/model/data/UserData.dart';

class TimeLinePostHelper {
  getParam({
    String message,
    String postTypeName,
    String additionalAttributeValue,
    List<dynamic> inlineTags,
    String checkin,
    int fromLat,
    int fromLng,
    String smallImageName,
    int receiverId,
    int ownerId,
    int taskId,
    bool isPrivate,
  }) {
    return {
      "Message": message,
      "PostTypeName": postTypeName,
      "AdditionalAttributeValue": additionalAttributeValue,
      "InlineTags": inlineTags,
      "Checkin": checkin,
      "FromLat": fromLat,
      "FromLng": fromLng,
      "SmallImageName": smallImageName,
      "ReceiverId": receiverId,
      "OwnerId": ownerId,
      "SenderId": userData.userModel.id,
      "TaskId": taskId,
      "IsPrivate": isPrivate,
    };
  }
}
