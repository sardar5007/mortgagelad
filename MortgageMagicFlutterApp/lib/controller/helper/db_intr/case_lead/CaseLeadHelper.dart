import 'package:aitl/model/data/UserData.dart';
import 'package:jiffy/jiffy.dart';

class CaseLeadHelper {
  getParam({
    String title,
    String fname,
    String lname,
    String mname,
    String mobile,
    String email,
    String note,
    int negotiatorId,
  }) {
    return {
      "Id": 0,
      "Status": 101,
      "Title": title,
      "Description": "",
      "Remarks": "",
      "InitiatorId": 115774,
      "ServiceDate": Jiffy().format("dd-MMM-yyyy"),
      "ResolutionType": "Lead From Introducer",
      "ParentId": 0,
      "AssigneeId": 0,
      "AssigneeName": "",
      "UserCompanyId": userData.userModel.userCompanyID.toString(),
      "LeadStatus": "New",
      "FirstName": fname.trim(),
      "MiddleName": mname.trim(),
      "LastName": lname.trim(),
      "MobileNumber": mobile.trim(),
      "Email": email.trim(),
      "DateofBirth": userData.userModel.dateofBirth,
      "ReferenceId": 0,
      "Stage": "Added",
      "Probability": 0,
      "TitleOthers": "",
      "EstimatedEarning": 0,
      "IsLockAutoCall": 0,
      "SupportAdminId": 0,
      "UserNoteEntityId": 0,
      "UserNoteEntityName": "",
      "NegotiatorId": negotiatorId.toString(),
      "PhoneNumber": mobile, //userData.userModel.mobileNumber,
      "EmailAddress": email, //userData.userModel.email,
      "LeadNote": note.trim(),
    };
  }
}
