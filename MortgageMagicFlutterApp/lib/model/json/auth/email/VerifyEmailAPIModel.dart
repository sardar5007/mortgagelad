class VerifyEmailAPIModel {
  bool success;
  _ErrorMessages errorMessages;
  _Messages messages;
  dynamic responseData;

  VerifyEmailAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});
  //LoginModel({this.success, this.errorMessages, this.messages});

  factory VerifyEmailAPIModel.fromJson(Map<String, dynamic> j) {
    return VerifyEmailAPIModel(
      success: j['Success'] as bool,
      errorMessages: _ErrorMessages.fromJson(j['ErrorMessages']) ?? [],
      messages: _Messages.fromJson(j['Messages']) ?? [],
      responseData: j['ResponseData'] ?? null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ErrorMessages {
  dynamic code;
  _ErrorMessages({this.code});
  factory _ErrorMessages.fromJson(Map<String, dynamic> j) {
    return _ErrorMessages(
      code: j['code'] ?? {},
    );
  }
  Map<String, dynamic> toMap() => {
        'code': code,
      };
}

class _Messages {
  dynamic code;
  _Messages({this.code});
  factory _Messages.fromJson(Map<String, dynamic> j) {
    return _Messages(
      code: j['code'] ?? {},
    );
  }
  Map<String, dynamic> toMap() => {
        'code': code,
      };
}
