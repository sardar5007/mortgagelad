import 'package:aitl/model/json/comp_acc/UserCompanyInfoModel.dart';

class CompAccAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  CompAccAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  CompAccAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  Messages();
  Messages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<UserCompanyInfoModel> userCompanyInfos;
  ResponseData({this.userCompanyInfos});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['UserCompanyInfos'] != null) {
      userCompanyInfos = [];
      json['UserCompanyInfos'].forEach((v) {
        userCompanyInfos.add(new UserCompanyInfoModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userCompanyInfos != null) {
      data['UserCompanyInfos'] =
          this.userCompanyInfos.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
