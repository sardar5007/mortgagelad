class UserCompanyInfoModel {
  String companyName;
  String companyType;
  String companyWebsite;
  String officePhoneNumber;
  String userType;
  int id;

  UserCompanyInfoModel(
      {this.companyName,
      this.companyType,
      this.companyWebsite,
      this.officePhoneNumber,
      this.userType,
      this.id});

  UserCompanyInfoModel.fromJson(Map<String, dynamic> json) {
    companyName = json['CompanyName'] ?? '';
    companyType = json['CompanyType'] ?? '';
    companyWebsite = json['CompanyWebsite'] ?? '';
    officePhoneNumber = json['OfficePhoneNumber'] ?? '';
    userType = json['UserType'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CompanyName'] = this.companyName;
    data['CompanyType'] = this.companyType;
    data['CompanyWebsite'] = this.companyWebsite;
    data['OfficePhoneNumber'] = this.officePhoneNumber;
    data['UserType'] = this.userType;
    data['Id'] = this.id;
    return data;
  }
}
