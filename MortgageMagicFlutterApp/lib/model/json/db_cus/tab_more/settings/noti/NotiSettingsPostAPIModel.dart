import 'NotificationSettingModel.dart';

class NotiSettingsPostAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  NotiSettingsPostAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  NotiSettingsPostAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  List<String> postNotificationSetting;
  Messages({this.postNotificationSetting});
  Messages.fromJson(Map<String, dynamic> json) {
    try {
      postNotificationSetting =
          json['POSTNotificationSetting'].cast<String>() ?? [];
    } catch (e) {
      postNotificationSetting = [];
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['POSTNotificationSetting'] = this.postNotificationSetting;
    return data;
  }
}

class ResponseData {
  NotiSettingModel userNotificationSetting;
  ResponseData({this.userNotificationSetting});
  ResponseData.fromJson(Map<String, dynamic> json) {
    userNotificationSetting = json['UserNotificationSetting'] != null
        ? new NotiSettingModel.fromJson(json['UserNotificationSetting'])
        : {};
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userNotificationSetting != null) {
      data['UserNotificationSetting'] = this.userNotificationSetting.toJson();
    }
    return data;
  }
}
