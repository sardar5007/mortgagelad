class ResolutionModel {
  int userId;
  String creationDate;
  int status;
  String title;
  String description;
  String remarks;
  String emailAddress;
  String phoneNumber;
  int initiatorId;
  String serviceDate;
  String resolutionType;
  int parentId;
  String profileImageUrl;
  String profileOwnerName;
  String complainee;
  String complaineeUrl;
  int assigneeId;
  String assigneeName;
  String assignDate;
  String resolvedDate;
  String taskStatus;
  String userComunity;
  String resolutionsUrl;
  int userCompanyId;
  String leadStatus;
  String leadNote;
  String firstName;
  String middleName;
  String namePrefix;
  String lastName;
  String initiatorImageUrl;
  String initiatorName;
  String initiatorCompanyName;
  String stage;
  int probability;
  String companyName;
  String fileUrl;
  String leadReferenceId;
  String dateOfBirth;
  String address;
  String mortgageLengthLeft;
  String homeValue;
  String mortgagePurpose;
  String mortgageAmount;
  String mortgageBorrowLength;
  String creditRating;
  String employmentStatus;
  String yearlyIncome;
  String emailConsent;
  String qualifier;
  double estimatedEarning;
  String forwardedCompanyName;
  int createdByUserId;
  String createdByUserName;
  int isLockAutoCall;
  int supportAdminId;
  String userNoteEntityName;
  int userNoteEntityId;
  int negotiatorId;
  String negotiatorName;
  int id;

  ResolutionModel({
    this.userId,
    this.creationDate,
    this.status,
    this.title,
    this.description,
    this.remarks,
    this.emailAddress,
    this.phoneNumber,
    this.initiatorId,
    this.serviceDate,
    this.resolutionType,
    this.parentId,
    this.profileImageUrl,
    this.profileOwnerName,
    this.complainee,
    this.complaineeUrl,
    this.assigneeId,
    this.assigneeName,
    this.assignDate,
    this.resolvedDate,
    this.taskStatus,
    this.userComunity,
    this.resolutionsUrl,
    this.userCompanyId,
    this.leadStatus,
    this.leadNote,
    this.firstName,
    this.middleName,
    this.namePrefix,
    this.lastName,
    this.initiatorImageUrl,
    this.initiatorName,
    this.initiatorCompanyName,
    this.stage,
    this.probability,
    this.companyName,
    this.fileUrl,
    this.leadReferenceId,
    this.dateOfBirth,
    this.address,
    this.mortgageLengthLeft,
    this.homeValue,
    this.mortgagePurpose,
    this.mortgageAmount,
    this.mortgageBorrowLength,
    this.creditRating,
    this.employmentStatus,
    this.yearlyIncome,
    this.emailConsent,
    this.qualifier,
    this.estimatedEarning,
    this.forwardedCompanyName,
    this.createdByUserId,
    this.createdByUserName,
    this.isLockAutoCall,
    this.supportAdminId,
    this.userNoteEntityName,
    this.userNoteEntityId,
    this.negotiatorId,
    this.negotiatorName,
    this.id,
  });

  factory ResolutionModel.fromJson(Map<String, dynamic> j) {
    return ResolutionModel(
      userId: j['UserId'] ?? 0,
      creationDate: j['CreationDate'] ?? '',
      status: j['Status'] ?? 0,
      title: j['Title'] ?? '',
      description: j['Description'] ?? '',
      remarks: j['Remarks'] ?? '',
      emailAddress: j['EmailAddress'] ?? '',
      phoneNumber: j['PhoneNumber'] ?? '',
      initiatorId: j['InitiatorId'] ?? 0,
      serviceDate: j['ServiceDate'] ?? '',
      resolutionType: j['ResolutionType'] ?? '',
      parentId: j['ParentId'] ?? 0,
      profileImageUrl: j['ProfileImageUrl'] ?? '',
      profileOwnerName: j['ProfileOwnerName'] ?? '',
      complainee: j['Complainee'] ?? '',
      complaineeUrl: j['ComplaineeUrl'] ?? '',
      assigneeId: j['AssigneeId'] ?? 0,
      assigneeName: j['AssigneeName'] ?? '',
      assignDate: j['AssignDate'] ?? '',
      resolvedDate: j['ResolvedDate'] ?? '',
      taskStatus: j['TaskStatus'] ?? '',
      userComunity: j['UserComunity'] ?? '',
      resolutionsUrl: j['ResolutionsUrl'] ?? '',
      userCompanyId: j['UserCompanyId'] ?? 0,
      leadStatus: j['LeadStatus'] ?? '',
      leadNote: j['LeadNote'] ?? '',
      firstName: j['FirstName'] ?? '',
      middleName: j['MiddleName'] ?? '',
      namePrefix: j['NamePrefix'] ?? '',
      lastName: j['LastName'] ?? '',
      initiatorImageUrl: j['InitiatorImageUrl'] ?? '',
      initiatorName: j['InitiatorName'] ?? '',
      initiatorCompanyName: j['InitiatorCompanyName'] ?? '',
      stage: j['Stage'] ?? '',
      probability: j['Probability'] ?? 0,
      companyName: j['CompanyName'] ?? '',
      fileUrl: j['FileUrl'] ?? '',
      leadReferenceId: j['LeadReferenceId'] ?? '',
      dateOfBirth: j['DateOfBirth'] ?? '',
      address: j['Address'] ?? '',
      mortgageLengthLeft: j['MortgageLengthLeft'] ?? '',
      homeValue: j['HomeValue'] ?? '',
      mortgagePurpose: j['MortgagePurpose'] ?? '',
      mortgageAmount: j['MortgageAmount'] ?? '',
      mortgageBorrowLength: j['MortgageBorrowLength'] ?? '',
      creditRating: j['CreditRating'] ?? '',
      employmentStatus: j['EmploymentStatus'] ?? '',
      yearlyIncome: j['YearlyIncome'] ?? '',
      emailConsent: j['EmailConsent'] ?? '',
      qualifier: j['Qualifier'] ?? '',
      estimatedEarning: j['EstimatedEarning'] ?? 0,
      forwardedCompanyName: j['ForwardedCompanyName'] ?? '',
      createdByUserId: j['CreatedByUserId'] ?? 0,
      createdByUserName: j['CreatedByUserName'] ?? '',
      isLockAutoCall: j['IsLockAutoCall'] ?? 0,
      supportAdminId: j['SupportAdminId'] ?? 0,
      userNoteEntityName: j['UserNoteEntityName'] ?? '',
      userNoteEntityId: j['UserNoteEntityId'] ?? 0,
      negotiatorId: j['NegotiatorId'] ?? 0,
      negotiatorName: j['NegotiatorName'] ?? '',
      id: j['Id'] ?? 0,
    );
  }

  Map<String, dynamic> toMap() => {
        'UserId': userId,
        'CreationDate': creationDate,
        'Status': status,
        'Title': title,
        'Description': description,
        'Remarks': remarks,
        'EmailAddress': emailAddress,
        'PhoneNumber': phoneNumber,
        'InitiatorId': initiatorId,
        'ServiceDate': serviceDate,
        'ResolutionType': resolutionType,
        'ParentId': parentId,
        'ProfileImageUrl': profileImageUrl,
        'ProfileOwnerName': profileOwnerName,
        'Complainee': complainee,
        'ComplaineeUrl': complaineeUrl,
        'AssigneeId': assigneeId,
        'AssigneeName': assigneeName,
        'AssignDate': assignDate,
        'ResolvedDate': resolvedDate,
        'TaskStatus': taskStatus,
        'UserComunity': userComunity,
        'ResolutionsUrl': resolutionsUrl,
        'UserCompanyId': userCompanyId,
        'LeadStatus': leadStatus,
        'LeadNote': leadNote,
        'FirstName': firstName,
        'MiddleName': middleName,
        'NamePrefix': namePrefix,
        'LastName': lastName,
        'InitiatorImageUrl': initiatorImageUrl,
        'InitiatorName': initiatorName,
        'InitiatorCompanyName': initiatorCompanyName,
        'Stage': stage,
        'Probability': probability,
        'CompanyName': companyName,
        'FileUrl': fileUrl,
        'LeadReferenceId': leadReferenceId,
        'DateOfBirth': dateOfBirth,
        'Address': address,
        'MortgageLengthLeft': mortgageLengthLeft,
        'HomeValue': homeValue,
        'MortgagePurpose': mortgagePurpose,
        'MortgageAmount': mortgageAmount,
        'MortgageBorrowLength': mortgageBorrowLength,
        'CreditRating': creditRating,
        'EmploymentStatus': employmentStatus,
        'YearlyIncome': yearlyIncome,
        'EmailConsent': emailConsent,
        'Qalifier': qualifier,
        'EstimatedEarning': estimatedEarning,
        'ForwardedCompanyName': forwardedCompanyName,
        'CreatedByUserId': createdByUserId,
        'CreatedByUserName': createdByUserName,
        'IsLockAutoCall': isLockAutoCall,
        'SupportAdminId': supportAdminId,
        'UserNoteEntityName': userNoteEntityName,
        'UserNoteEntityId': userNoteEntityId,
        'NegotiatorId': negotiatorId,
        'NegotiatorName': negotiatorName,
        'Id': id,
      };
}
