class FCMDeviceInfoModel {
  int userId;
  String creationDate;
  String deviceType;
  String deviceId;
  String deviceToken;
  String browserName;
  String versionNumber;
  int id;

  FCMDeviceInfoModel(
      {this.userId,
      this.creationDate,
      this.deviceType,
      this.deviceId,
      this.deviceToken,
      this.browserName,
      this.versionNumber,
      this.id});
  FCMDeviceInfoModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    creationDate = json['CreationDate'];
    deviceType = json['DeviceType'];
    deviceId = json['DeviceId'];
    deviceToken = json['DeviceToken'];
    browserName = json['BrowserName'];
    versionNumber = json['VersionNumber'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['CreationDate'] = this.creationDate;
    data['DeviceType'] = this.deviceType;
    data['DeviceId'] = this.deviceId;
    data['DeviceToken'] = this.deviceToken;
    data['BrowserName'] = this.browserName;
    data['VersionNumber'] = this.versionNumber;
    data['Id'] = this.id;
    return data;
  }
}
