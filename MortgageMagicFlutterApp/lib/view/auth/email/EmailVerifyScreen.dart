import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/auth/email/EmailVerifyAPIMgr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/view/auth/RegScreen.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/OTPTextField.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../Mixin.dart';

class EmailVerifyScreen extends StatefulWidget {
  final String email;
  const EmailVerifyScreen({Key key, @required this.email}) : super(key: key);
  @override
  State createState() => _EmailVerifyScreenState();
}

class _EmailVerifyScreenState extends State<EmailVerifyScreen> with Mixin {
  bool isLoading = false;
  String code = '';

  emailVerifyAPI() {
    EmailVerifyAPIMgr().wsEmailVerifyAPI(
      context: context,
      code: code,
      callback: (model) {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                final msg = model.messages.code[0].toString();
                showToast(msg: msg, which: 1);
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                if (mounted) {
                  final err = model.errorMessages.code[0].toString();
                  showToast(msg: err);
                }
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          titleSpacing: 0,
          title: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              width: 100,
              height: 60,
              child: Image.asset(
                "assets/images/icons/back_btn_icon.png",
              ),
            ),
          ),
          actions: [
            TextButton(
              //textColor: Colors.white,
              onPressed: () {
                navTo(context: context, isRep: true, page: () => RegScreen());
              },
              child: Txt(
                  txt: "Sign up",
                  txtColor: MyTheme.redColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              //shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
          ],
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10, top: 0, bottom: 20),
      child: Container(
        width: getW(context),
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 20),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  child: Txt(
                    txt: "Verify your mail",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize + 1.3,
                    txtAlign: TextAlign.start,
                    isBold: true,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, top: 10, bottom: 10),
                  child: Txt(
                    txt: "Please enter the 4 digit code",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  child: Txt(
                    txt: "It will be sending to " + widget.email,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                ),
                SizedBox(height: 40),
                Center(
                  child: Container(
                    //color: Colors.black,
                    child: Theme(
                      data: ThemeData(
                        //primaryColor: Colors.orangeAccent,
                        //primaryColorDark: Colors.orange,
                        backgroundColor: MyTheme.redColor,
                      ),
                      child: OTPTextField(
                        length: 4,
                        width: getWP(context, 90),
                        textFieldAlignment: MainAxisAlignment.spaceAround,
                        fieldWidth: getWP(context, 80) / 5,
                        fieldStyle: FieldStyle.box,
                        style: TextStyle(
                          fontSize: getTxtSize(
                              context: context, txtSize: MyTheme.txtSize + 2),
                          color: Colors.white,
                          fontWeight: FontWeight.normal,
                        ),
                        onChanged: (pin) {
                          log("Changed: " + pin);
                          code = pin;
                        },
                        onCompleted: (pin) {
                          log("Completed: " + pin);
                          code = pin;
                          emailVerifyAPI();
                        },
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 10, right: 10, top: 40),
                    child: GestureDetector(
                      onTap: () {
                        /* navTo(
          context: context,
          isRep: true,
          page: () =>(ForgotScreen());*/
                      },
                      child: Txt(
                          txt: "Resend code",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .1,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 40, bottom: 20),
                  child: MMBtn(
                      txt: "Confirm",
                      height: getHP(context, 7),
                      width: getW(context),
                      callback: () {
                        if (code.length != 4) {
                          showToast(msg: "Invalid code entered");
                        } else {
                          emailVerifyAPI();
                        }
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
