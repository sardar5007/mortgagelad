import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/auth/LoginScreen.dart';
import 'package:aitl/view/mywidgets/IcoTxtIco.dart';
import 'package:aitl/view/mywidgets/TCView.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:flutter/services.dart';
import 'Sms2Screen.dart';

class Sms1Screen extends StatefulWidget {
  @override
  State createState() => _Sms1ScreenState();
}

class _Sms1ScreenState extends State<Sms1Screen> with Mixin {
  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() {
    try {
      SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(
            statusBarColor: MyTheme.redColor,
            statusBarIconBrightness: Brightness.dark),
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.redColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      navTo(
                          context: context,
                          isRep: true,
                          page: () => LoginScreen());
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, size: 20),
                        Txt(
                            txt: "Back",
                            txtColor: Colors.white,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.redColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return ListView(
      shrinkWrap: true,
      children: [
        SizedBox(height: getHP(context, 5)),
        Padding(
          padding: const EdgeInsets.only(left: 40, right: 40),
          child: Txt(
            txt: "Please give your mobile number to start using the app",
            txtColor: Colors.white,
            txtSize: MyTheme.txtSize + .2,
            txtAlign: TextAlign.center,
            //txtLineSpace: 1.2,
            isBold: false,
          ),
        ),
        drawCenterMobileCode(),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: TCView(
            screenName: 'Log in',
            txt1Color: Colors.white,
            txt2Color: Colors.yellow,
          ),
        ),
      ],
    );
  }

  drawCenterMobileCode() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        height: getHP(context, 45),
        width: getWP(context, 80),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(20.0),
            topRight: const Radius.circular(20.0),
            bottomLeft: const Radius.circular(20.0),
            bottomRight: const Radius.circular(20.0),
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Txt(
                txt: "Please give your mobile number",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: true,
              ),
            ),
            GestureDetector(
              onTap: () async {
                navTo(
                  context: context,
                  page: () => Sms2Screen(),
                ).then((value) {
                  navTo(
                      context: context, isRep: true, page: () => LoginScreen());
                });
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: IcoTxtIco(
                  leftIcon: Icons.mobile_screen_share,
                  txt: "Log in with Mobile",
                  rightIcon: Icons.arrow_right,
                  iconColor: Colors.black54,
                  leftIconSize: 40,
                  rightIconSize: 50,
                ),
              ),
            ),
            SizedBox(height: getHP(context, 5)),
            Expanded(
              child: Container(
                width: getWP(context, 80),
                //height: getHP(context, 20),
                child: Image.asset(
                  'assets/images/screens/login/login_mobile_bg.png',
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
