import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/mywidgets/AppbarBotProgbar.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';

Widget MyCasesAppbar(
    {BuildContext context,
    TabController tabController,
    double h,
    bool isLoading,
    Function callback,
    Function callbackWebView}) {
  return AppBar(
    elevation: MyTheme.appbarElevation,
    backgroundColor: MyTheme.themeData.accentColor,
    title: Txt(
        txt: "My Cases",
        txtColor: MyTheme.redColor,
        txtSize: MyTheme.appbarTitleFontSize,
        txtAlign: TextAlign.start,
        isBold: true),
    centerTitle: true,
    actions: <Widget>[
      IconButton(
        iconSize: 30,
        icon: Image.asset("assets/images/icons/help_circle_icon.png"),
        onPressed: () {
          // do something
          callbackWebView();
        },
      )
    ],
    bottom: PreferredSize(
      preferredSize: Size.fromHeight(h * 12 / 100),
      child: Column(
        children: [
          Container(
            color: MyTheme.redColor,
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, top: 15, bottom: 15),
              child: Center(
                child: Txt(
                    txt: "View the ongoing cases",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
            ),
          ),
          TabBar(
            onTap: (index2) {
              if (isLoading) {
                callback(index2);
              }
            },
            controller: tabController,
            isScrollable: true,
            indicatorColor: MyTheme.redColor,
            unselectedLabelColor: Colors.black54,
            labelColor: MyTheme.redColor,
            /*indicator: UnderlineTabIndicator(
                              borderSide:
                                  BorderSide(width: 5.0, color: Colors.white),
                              insets: EdgeInsets.symmetric(horizontal: 16.0)),*/
            tabs: [
              Tab(
                  child: Txt(
                      txt: "All",
                      txtColor: null,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: true)),
              Tab(
                  child: Txt(
                      txt: "In-Progress",
                      txtColor: null,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: true)),
              Tab(
                  child: Txt(
                      txt: "Submitted",
                      txtColor: null,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: true)),
              Tab(
                  child: Txt(
                      txt: "FMA Submitted",
                      txtColor: null,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: true)),
              Tab(
                  child: Txt(
                      txt: "Completed",
                      txtColor: null,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: true)),
            ],
          ),
          (isLoading)
              ? AppbarBotProgBar(
                  backgroundColor: MyTheme.appbarProgColor,
                )
              : Container()
        ],
      ),
    ),
  );
}
