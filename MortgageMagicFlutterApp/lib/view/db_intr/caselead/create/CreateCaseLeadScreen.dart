import 'dart:convert';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/controller/api/db_intr/case_lead/CaseLeadNavigatorAPIMgr.dart';
import 'package:aitl/controller/form_validate/UserProfileVal.dart';
import 'package:aitl/model/json/db_cus/tab_more/support/ResolutionAPIModel.dart';
import 'package:aitl/view/mywidgets/InputTitleBox.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/dropdown/DropDownPicker.dart';
import 'package:aitl/view/mywidgets/dropdown/DropListModel.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/material.dart';
import 'package:aitl/model/json/db_cus/case_lead/CaseLeadNegotiatorUserModel.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:aitl/controller/helper/db_intr/case_lead/CaseLeadHelper.dart';

class CreateCaseLeadScreen extends StatefulWidget {
  /*final UserNotesModel userNotesModel;
  const CreateCaseLeadScreen({Key key, @required this.userNotesModel})
      : super(key: key);*/
  @override
  State createState() => _CreateCaseLeadScreenState();
}

class _CreateCaseLeadScreenState extends State<CreateCaseLeadScreen>
    with Mixin {
  final _fname = TextEditingController();
  final _mname = TextEditingController();
  final _lname = TextEditingController();
  final _email = TextEditingController();
  final _mobile = TextEditingController();
  final _note = TextEditingController();
  final _othersLeadTYpe = TextEditingController();

  //  dropdown
  DropListModel dd = DropListModel([]);
  OptionItem opt = OptionItem(id: null, title: "Select Negotiator");

  List<Map<String, dynamic>> listCreateNewCase;
  int indexCase = -1;
  bool isShowOthersLeadTYpe = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _email.dispose();
    _mname.dispose();
    _fname.dispose();
    _lname.dispose();
    _mobile.dispose();
    _note.dispose();
    _othersLeadTYpe.dispose();
    opt = null;
    dd = null;
    listCreateNewCase = null;
    super.dispose();
  }

  validate() {
    if (indexCase == -1) {
      showToast(msg: "Please select 'Lead Type' from the above options");
      return false;
    }
    if (isShowOthersLeadTYpe &&
        UserProfileVal().isEmpty(_othersLeadTYpe, "PLease enter lead title")) {
      return false;
    }
    if (!UserProfileVal().isFNameOK(_fname)) {
      return false;
    }
    if (!UserProfileVal().isLNameOK(_lname)) {
      return false;
    }
    if (!UserProfileVal().isEmailOK(_email, "Invalid email address")) {
      return false;
    }
    if (opt.id == null) {
      showToast(msg: "Please choose 'Negotiator' from the list");
      return false;
    }
    return true;
  }

  appInit() async {
    try {
      listCreateNewCase = NewCaseCfg.listCreateNewCase;
      NewCaseCfg().addOtherTitleInCase();

      CaseLeadNavigatorAPIMgr().wsGetCaseLeadNavigatorByIntroducerID(
        context: context,
        //userNotesModel: widget.userNotesModel,
        callback: (model) {
          if (mounted) {
            if (model.success) {
              final List<CaseLeadNegotiatorUserModel>
                  listCaseLeadNegotiatorUserModel = model.responseData.users;
              for (CaseLeadNegotiatorUserModel caseLeadNegotiatorUserModel
                  in listCaseLeadNegotiatorUserModel) {
                dd.listOptionItems.add(OptionItem(
                    id: caseLeadNegotiatorUserModel.id.toString(),
                    title: caseLeadNegotiatorUserModel.name));
              }
              setState(() {});
            } else {}
          }
        },
      );
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          centerTitle: true,
          elevation: MyTheme.appbarElevation,
          automaticallyImplyLeading: false,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                      //Get.off
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, size: 20),
                        Txt(
                            txt: "Back",
                            txtColor: MyTheme.redColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Txt(
                      txt: "Create Lead",
                      txtColor: MyTheme.redColor,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.center,
                      isBold: true),
                ),
                IconButton(
                  iconSize: 30,
                  icon: Image.asset("assets/images/icons/help_circle_icon.png"),
                  onPressed: () {
                    // do something
                    Get.to(
                      () => WebScreen(
                        title: "Help",
                        url: Server.HELP_INFO_URL,
                      ),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                )
              ],
            ),
          ),
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            drawCasesRadio(),
            drawInputBoxes(),
            Padding(
              padding: const EdgeInsets.only(
                  top: 30, left: 30, right: 30, bottom: 50),
              child: MMBtn(
                txt: "Save",
                height: getHP(context, 7),
                width: getW(context),
                callback: () {
                  if (validate()) {
                    final map = listCreateNewCase[indexCase];
                    String title = map['title'];
                    if (isShowOthersLeadTYpe) {
                      title = _othersLeadTYpe.text.trim();
                    }
                    //log(map['title']);
                    final param = CaseLeadHelper().getParam(
                      title: title,
                      fname: _fname.text.trim(),
                      mname: _mname.text.trim(),
                      lname: _lname.text.trim(),
                      email: _email.text.trim(),
                      mobile: _mobile.text.trim(),
                      note: _note.text.trim(),
                      negotiatorId: int.parse(opt.id),
                    );
                    log(json.encode(param));
                    CaseLeadNavigatorAPIMgr().wsResolutionAPI(
                      context: context,
                      param: param,
                      callback: (model) {
                        if (model != null && mounted) {
                          try {
                            if (model.success) {
                              _fname.text = "";
                              _mname.text = "";
                              _lname.text = "";
                              _email.text = "";
                              _mobile.text = "";
                              _note.text = "";
                              _othersLeadTYpe.text = "";
                              indexCase = -1;
                              final msg =
                                  model.messages.resolution_post[0].toString();
                              showToast(msg: msg, which: 1);
                              setState(() {});
                            } else {
                              final err = model.errorMessages.resolution_post[0]
                                  .toString();
                              showToast(msg: err, which: 0);
                              setState(() {});
                            }
                          } catch (e) {
                            log(e.toString());
                          }
                        }
                      },
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawCasesRadio() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 30, right: 30, top: 30),
          child: Txt(
              txt: "Lead Type",
              txtColor: MyTheme.timelineTitleColor,
              txtSize: MyTheme.txtSize + .5,
              txtAlign: TextAlign.start,
              isBold: true),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10, left: 30, right: 30),
          child: Divider(color: Colors.grey, height: 20),
        ),
        ListView.builder(
          primary: false,
          shrinkWrap: true,
          itemCount: listCreateNewCase.length,
          itemBuilder: (BuildContext context, int index) {
            final map = listCreateNewCase[index];
            return Padding(
              padding:
                  const EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
              child: Container(
                //color: Colors.black,
                decoration: MyTheme.boxDeco,
                child: Theme(
                  data: MyTheme.radioThemeData,
                  child: RadioListTile<int>(
                    title: Txt(
                        txt: map['title'],
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: true),
                    value: index,
                    groupValue: indexCase,
                    onChanged: (int value) {
                      setState(() {
                        indexCase = value;
                        if (indexCase == 9) {
                          isShowOthersLeadTYpe = true;
                          setState(() {});
                        } else {
                          isShowOthersLeadTYpe = false;
                          setState(() {});
                        }
                      });
                    },
                  ),
                ),
              ),
            );
          },
        ),
        (isShowOthersLeadTYpe)
            ? Padding(
                padding: const EdgeInsets.only(left: 25, right: 25, top: 10),
                child: Container(
                  //color: Colors.black,
                  //decoration: MyTheme.boxDeco,
                  child: drawInputBox(
                      title: "Lead Title",
                      input: _othersLeadTYpe,
                      kbType: TextInputType.text,
                      len: 100),
                ),
              )
            : SizedBox(),
      ],
    );
  }

  drawInputBoxes() {
    return Padding(
      padding: const EdgeInsets.only(top: 30, left: 30, right: 30),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: "Lead Informations",
                txtColor: MyTheme.timelineTitleColor,
                txtSize: MyTheme.txtSize + .5,
                txtAlign: TextAlign.start,
                isBold: true),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Divider(color: Colors.grey, height: 20),
            ),
            drawInputBox(
                title: "First Name",
                input: _fname,
                kbType: TextInputType.name,
                len: 20),
            drawInputBox(
                title: "Middle Name",
                input: _mname,
                kbType: TextInputType.name,
                len: 20),
            drawInputBox(
                title: "Last Name",
                input: _lname,
                kbType: TextInputType.name,
                len: 20),
            drawInputBox(
                title: "Mobile Number",
                input: _mobile,
                kbType: TextInputType.phone,
                len: 20),
            drawInputBox(
                title: "Email",
                input: _email,
                kbType: TextInputType.emailAddress,
                len: 50),
            drawInputBox(
                title: "Lead Notes",
                input: _note,
                kbType: TextInputType.text,
                len: 255),
            (dd.listOptionItems.length > 0)
                ? DropDownPicker(
                    cap: "Negotiator",
                    itemSelected: opt,
                    dropListModel: dd,
                    onOptionSelected: (optionItem) {
                      opt = optionItem;
                      setState(() {});
                    },
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
