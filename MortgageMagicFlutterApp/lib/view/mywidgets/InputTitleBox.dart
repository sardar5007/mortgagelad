import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';

import 'InputBox.dart';
import 'Txt.dart';

drawInputBox({
  @required String title,
  @required TextEditingController input,
  @required TextInputType kbType,
  @required int len,
  bool isPwd = false,
  bool autofocus = false,
}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Txt(
          txt: title,
          txtColor: Colors.black,
          txtSize: MyTheme.txtSize,
          txtAlign: TextAlign.start,
          isBold: true),
      SizedBox(height: 15),
      InputBox(
        ctrl: input,
        lableTxt: title,
        kbType: kbType,
        len: len,
        isPwd: isPwd,
        autofocus: autofocus,
      ),
    ],
  );
}
