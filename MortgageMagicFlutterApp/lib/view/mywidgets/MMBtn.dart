import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MMBtn extends StatelessWidget with Mixin {
  final String txt;
  Color bgColor;
  final double width;
  final double height;
  final double radius;
  final Function callback;

  MMBtn({
    Key key,
    @required this.txt,
    @required this.width,
    @required this.height,
    this.radius = 10,
    @required this.callback,
    this.bgColor,
  }) {
    if (bgColor == null) bgColor = MyTheme.redColor;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        callback();
      },
      child: Container(
        width: width,
        height: (height != null) ? height : getHP(context, 6),
        alignment: Alignment.center,
        //color: MyTheme.brownColor,
        decoration: new BoxDecoration(
          color: bgColor,
          borderRadius: new BorderRadius.circular(radius),
        ),
        child: Txt(
          txt: txt,
          txtColor: Colors.white,
          txtSize: MyTheme.txtSize + .3,
          txtAlign: TextAlign.center,
          isBold: false,
        ),
      ),
    );
  }
}
