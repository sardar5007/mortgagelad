import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:get/get.dart';

class TCView extends StatelessWidget with Mixin {
  final screenName;
  Color txt1Color;
  Color txt2Color;

  TCView({
    @required this.screenName,
    this.txt1Color = Colors.black,
    this.txt2Color,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(
                  text:
                      "By clicking you confirm that you accept the MortgageLad ",
                  style: TextStyle(
                      height: MyTheme.txtLineSpace,
                      color: txt1Color,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize)),
                  children: <TextSpan>[
                    TextSpan(
                        text: 'Terms and Conditions',
                        style: TextStyle(
						height: MyTheme.txtLineSpace,
                            color: (txt2Color == null)
                                ? MyTheme.redColor
                                : txt2Color,
                            fontSize: getTxtSize(
                                context: context, txtSize: MyTheme.txtSize),
                            fontWeight: FontWeight.bold),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            // navigate to desired screen
                            Get.to(
                              () => WebScreen(
                                title: "Terms & Conditions",
                                url: Server.TC_URL,
                              ),
                            ).then((value) {
                              //callback(route);
                            });
                          })
                  ]),
            ),
          ),
        ]);
  }
}
