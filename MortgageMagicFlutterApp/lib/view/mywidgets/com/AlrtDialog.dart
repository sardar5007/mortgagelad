import 'dart:ui';
import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../Mixin.dart';
import '../Txt.dart';

class AlrtDialog extends StatefulWidget {
  final int which;
  final String msg;
  const AlrtDialog({
    Key key,
    @required this.which,
    @required this.msg,
  }) : super(key: key);
  @override
  State createState() => _AlertDlgState();
}

class _AlertDlgState extends State<AlrtDialog> with Mixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: MyTheme.redColor,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20),
                child: Txt(
                  txt: widget.msg,
                  txtColor: Colors.white,
                  txtSize: MyTheme.txtSize + .3,
                  txtAlign: TextAlign.center,
                  isBold: true,
                  txtLineSpace: 1.5,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: getWP(context, 25), right: getWP(context, 25)),
                child: MaterialButton(
                  onPressed: () {
                    Get.back();
                  },
                  child: Txt(
                      txt: "Ok",
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  color: Colors.grey,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
